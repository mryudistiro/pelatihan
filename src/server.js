// server.js
import express from 'express';
import 'babel-polyfill';
import ReflectionJSObject from '../src/usingJSOBject/controllers/Reflection';
import RegisterUsingDB from '../src/usingDB/controllers/Register';

const app = express()

app.use(express.json())

app.get('/', (req, res) => {
  return res.status(200).send({'message': 'YAY! Congratulations! Your first endpoint is working 3010 Yudi'});
})
app.post('/api/v1/registers', RegisterUsingDB.create);
app.get('/api/v1/registers', RegisterUsingDB.getAll);
app.get('/api/v1/registers/:id', RegisterUsingDB.getOne);
app.put('/api/v1/registers/:id', RegisterUsingDB.update);
app.delete('/api/v1/registers/:id', RegisterUsingDB.delete);

app.post('/api/v1/register',RegisterUsingDB.registerNew)

app.post('/api/v1/login',RegisterUsingDB.loginNew)
app.listen(3010)

console.log('app running on port ', 3010);