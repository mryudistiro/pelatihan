// src/usingDB/controllers/Reflection.js
import moment from 'moment';
import { v4 as uuidv4 } from 'uuid';
import db from '../db/index';
import bcrypt, { compare } from 'bcryptjs'
import jwt from 'jsonwebtoken'

const Register = {
    /**
     * Create A Register
     * @param {object} req 
     * @param {object} res
     * @returns {object} Register object 
     */
    async create(req, res) {
      const createQuery = `INSERT INTO
        pegawais(id, email, nama, nip, password, created_date, modified_date )
        VALUES($1, $2, $3, $4, $5, $6, $7)
        returning *`;
      const values = [
        uuidv4(),
        req.body.nama,
        req.body.email,
        req.body.nip,
        req.body.password,
        req.user.id,
        moment(new Date()),
        moment(new Date())
      ];
  
      try {
        const { rows } = await db.query(createQuery, values);
        return res.status(201).send(rows[0]);
      } catch(error) {
        return res.status(400).send(error);
      }
    },
    /**
     * Get All Pegawais
     * @param {object} req 
     * @param {object} res 
     * @returns {object} reflections array
     */
    async getAll(req, res) {
      const findAllQuery = 'SELECT * FROM pegawais where owner_id = $1';
      try {
        const { rows, rowCount } = await db.query(findAllQuery, [req.user.id]);
        return res.status(200).send({ rows, rowCount });
      } catch(error) {
        return res.status(400).send(error);
      }
    },
    /**
     * Get A Reflection
     * @param {object} req 
     * @param {object} res
     * @returns {object} reflection object
     */
    async getOne(req, res) {
      const text = 'SELECT * FROM pegawais WHERE id = $1 AND owner_id = $2';
      try {
        const { rows } = await db.query(text, [req.params.id, req.user.id]);
        if (!rows[0]) {
          return res.status(404).send({'message': '[pegawai] not found'});
        }
        return res.status(200).send(rows[0]);
      } catch(error) {
        return res.status(400).send(error)
      }
    },
    /**
     * Update A Reflection
     * @param {object} req 
     * @param {object} res 
     * @returns {object} updated reflection
     */
    async update(req, res) {
      const findOneQuery = 'SELECT * FROM pegawais WHERE id=$1 AND owner_id = $2';
      const updateOneQuery =`UPDATE [pegawais]
        SET email=$2,nama=$3,nip=$4
        WHERE id=$5 AND owner_id = $6 returning *`;
      try {
        const { rows } = await db.query(findOneQuery, [req.params.id, req.user.id]);
        if(!rows[0]) {
          return res.status(404).send({'message': 'reflection not found'});
        }
        const values = [
          req.body.nama || rows[0].nama,
          req.body.nip || rows[0].nip,
          req.body.email || rows[0].email,
          moment(new Date()),
          req.params.id,
          req.user.id
        ];
        const response = await db.query(updateOneQuery, values);
        return res.status(200).send(response.rows[0]);
      } catch(err) {
        return res.status(400).send(err);
      }
    },
    /**
     * Delete A Reflection
     * @param {object} req 
     * @param {object} res 
     * @returns {void} return statuc code 204 
     */
    async delete(req, res) {
      const deleteQuery = 'DELETE FROM pegawais WHERE id=$1 AND owner_id = $2 returning *';
      try {
        const { rows } = await db.query(deleteQuery, [req.params.id, req.user.id]);
        if(!rows[0]) {
          return res.status(404).send({'message': 'pegawai not found'});
        }
        return res.status(204).send({ 'message': 'deleted' });
      } catch(error) {
        return res.status(400).send(error);
      }
    },

  async registerNew(req, res, next) {
    const { name, pekerjaan, email, password, nip } = req.body;
    const querynip =`select * from pegawai where nip='${nip}'`
    const queryusername =`select * from pegawai where email='${email}'`
    const hashPassword = bcrypt.hashSync(password, 10)
    const id = uuidv4()
    try {
      const isNipExist = await db.query(querynip);
      const isEmailExist = await db.query(queryusername);
      if (isNipExist['rowCount']>0) {
        throw Object.assign(new Error('NIP telah digunakan'), { code: 400 });
      } else if(isEmailExist ['rowCount']>0) {
        throw Object.assign(new Error('Email telah digunakan'), { code: 400 });
      } else {
        const queryInsert = `INSERT INTO pegawai (id, nip, email , nama , pekerjaan,"password")
        VALUES ('${id}','${nip}', '${email}', '${name}', '${pekerjaan}', '${hashPassword}');` 
        const { result } = await db.query(queryInsert);
        res.status(200).send({'message':'Berhasil Register'});
        }
    } catch (error) {
      res.status(error.code || 400).send({ message: error.message });
    }
  },

  async loginNew(req, res) {
   const {email, password} = req.body
   const query = `select * from pegawai where email='${email}'`
   const {rows} = await db.query(query)
   if (rows && bcrypt.compareSync(password, rows[0].password)){
    var token = jwt.sign({email: rows[0].email, nip: rows[0].nip, nama: rows[0].nama}, 'shhhhh', {expiresIn: 1 * 60 });
    console.log(token)
   } else {
       console.log('Salah') 
   }
  }  

}
  

  export default Register