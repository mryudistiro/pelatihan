
// db.js
const { Pool } = require('pg');
const dotenv = require('dotenv');

dotenv.config();

const pool = new Pool({
  connectionString: process.env.DATABASE_URL
});

pool.on('connect', () => {
  console.log('connected to the db');
});

/**
 * Create Tabel Pegawai
 */
const createPegawaiTable = () => {
  const queryText =
  `CREATE TABLE IF NOT EXISTS
  pegawai(
    id UUID PRIMARY KEY,
    email VARCHAR(128) UNIQUE NOT NULL,
    password VARCHAR(128) NOT NULL,
    nip VARCHAR(20) NOT NULL,
    nama VARCHAR(128) NOT NULL,
    pekerjaan VARCHAR(128) NOT NULL,
    created_date TIMESTAMP,
    modified_date TIMESTAMP
  )`;

  pool.query(queryText)
    .then((res) => {
      console.log(res);
      pool.end();
    })
    .catch((err) => {
      console.log(err);
      pool.end();
    });
}


/**
 * Drop Table Pegawai
 */
const dropPegawaiTable = () => {
  const queryText = 'DROP TABLE IF EXISTS pegawais returning *';
  pool.query(queryText)
    .then((res) => {
      console.log(res);
      pool.end();
    })
    .catch((err) => {
      console.log(err);
      pool.end();
    });
}

/**
 * Create All Tables
 */
const createAllTables = () => {
  createPegawaiTable();
}
/**
 * Drop All Tables
 */
const dropAllTables = () => {
  dropPegawaiTable();
}

pool.on('remove', () => {
  console.log('client removed');
  process.exit(0);
});


module.exports = {
  createPegawaiTable,
  createAllTables,
  dropPegawaiTable,
  dropAllTables
};

require('make-runnable');